package ru.lanolin.compmath.l2.utils;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import ru.lanolin.compmath.l2.integrals.Integral;
import ru.lanolin.compmath.l2.methods.Method;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class Parameters implements Serializable, Cloneable {

	/**
	 * Верхняя граница
	 */
	private BigInteger a;

	/**
	 * Нижняя граница
	 */
	private BigInteger b;

	/**
	 * Точность вычисления
	 */
	private BigDecimal accuracy;

	/**
	 * Число разделений
	 */
	private BigInteger n = BigInteger.valueOf(4);

	/**
	 * Метод, которым надо  решать интеграл
	 */
	private Method method;

	/**
	 *Предустановленный интеграл
	 */
	private Integral integral;
}
