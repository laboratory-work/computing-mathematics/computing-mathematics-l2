package ru.lanolin.compmath.l2.methods;

import ru.lanolin.compmath.l2.integrals.Integral;

import java.math.BigDecimal;
import java.math.MathContext;

public class MethodAvRectangles implements Method {

	@Override
	public BigDecimal area(Integral integral, BigDecimal start, BigDecimal end, BigDecimal h) {
		BigDecimal ans = BigDecimal.ZERO;
		BigDecimal half_h = h.divide(BigDecimal.valueOf(2), MathContext.DECIMAL128);

		for (BigDecimal xi = start; xi.compareTo(end) < 0; xi = xi.add(h)){
			ans = ans.add(integral.solve(xi.add(half_h)));
		}

		return ans.multiply(h);
	}

	@Override
	public String toString() {
		return "Метод средних прямоугольников";
	}
}
