package ru.lanolin.compmath.l2.methods;

import ru.lanolin.compmath.l2.integrals.Integral;

import java.math.BigDecimal;
import java.math.MathContext;

public class MethodSimpson implements Method {

	@Override
	public BigDecimal area(Integral integral, BigDecimal start, BigDecimal end, BigDecimal h) {
		BigDecimal ans = BigDecimal.ZERO;
		final BigDecimal h_3 = h.divide(BigDecimal.valueOf(3), MathContext.DECIMAL128);
		final BigDecimal h2 = h.multiply(BigDecimal.valueOf(2));
		final BigDecimal nH = h.negate();
		final BigDecimal four = BigDecimal.valueOf(4);

		for (BigDecimal xi = start.add(h); xi.compareTo(end) < 0; xi = xi.add(h2)){
			BigDecimal int_xi0 = integral.solve(xi.add(nH));
			BigDecimal int_xi = integral.solve(xi);
			BigDecimal int_xi1 = integral.solve(xi.add(h));

			ans = ans.add(int_xi0.add(int_xi.multiply(four)).add(int_xi1));
		}

		return ans.multiply(h_3);
	}

	@Override
	public String toString() {
		return "Метод Симпсона";
	}
}
