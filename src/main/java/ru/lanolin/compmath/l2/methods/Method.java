package ru.lanolin.compmath.l2.methods;

import ru.lanolin.compmath.l2.integrals.Integral;

import java.math.BigDecimal;

@FunctionalInterface
public interface Method {
	BigDecimal area(Integral integral, BigDecimal start, BigDecimal end, BigDecimal h);
}
