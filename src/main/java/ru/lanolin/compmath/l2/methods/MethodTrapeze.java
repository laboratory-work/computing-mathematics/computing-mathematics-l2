package ru.lanolin.compmath.l2.methods;

import ru.lanolin.compmath.l2.integrals.Integral;

import java.math.BigDecimal;
import java.math.MathContext;

public class MethodTrapeze implements Method {

	@Override
	public BigDecimal area(Integral integral, BigDecimal start, BigDecimal end, BigDecimal h) {
		BigDecimal ans = BigDecimal.ZERO;
		BigDecimal two = BigDecimal.valueOf(2);

		for (BigDecimal xi = start; xi.compareTo(end) < 0; xi = xi.add(h)){
			BigDecimal xii = xi.add(h);
			ans = ans.add(
					integral.solve(xi).add(integral.solve(xii))
			);
		}

		return ans.multiply(h).divide(two, MathContext.DECIMAL128);
	}

	@Override
	public String toString() {
		return "Метод трапеций";
	}
}
