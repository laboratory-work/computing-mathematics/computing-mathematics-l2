package ru.lanolin.compmath.l2.methods;

public class Methods {

	public static final int MAX_Methods = 5;

	public static String getMethodString(int number){
		return getMethod(number).toString();
	}

	public static Method getMethod(int number){
		switch (number){
			default:
			case 1:
				return new MethodLeftRectangles();
			case 2:
				return new MethodAvRectangles();
			case 3:
				return new MethodRightRectangles();
			case 4:
				return new MethodTrapeze();
			case 5:
				return new MethodSimpson();
		}
	}
}
