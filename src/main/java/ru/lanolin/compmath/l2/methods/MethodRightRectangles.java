package ru.lanolin.compmath.l2.methods;

import ru.lanolin.compmath.l2.integrals.Integral;

import java.math.BigDecimal;

public class MethodRightRectangles implements Method {

	@Override
	public BigDecimal area(Integral integral, BigDecimal start, BigDecimal end, BigDecimal h) {
		BigDecimal ans = BigDecimal.ZERO;

		for (BigDecimal xi = start.add(h); xi.compareTo(end) < 1; xi = xi.add(h)){
			ans = ans.add(integral.solve(xi));
		}

		return ans.multiply(h);
	}

	@Override
	public String toString() {
		return "Метод правых треугольников";
	}
}
