package ru.lanolin.compmath.l2;

import ru.lanolin.compmath.l2.implement.InputConsoleImpl;
import ru.lanolin.compmath.l2.solvers.Solver;
import ru.lanolin.compmath.l2.utils.Parameters;

public class Main {

	public static void main(String[] args) {
		Parameters params = InputConsoleImpl.getInputConsole().inputConsole();
		Solver.solve(params);
	}

}
