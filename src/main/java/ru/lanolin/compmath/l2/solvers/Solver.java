package ru.lanolin.compmath.l2.solvers;

import ru.lanolin.compmath.l2.utils.Parameters;
import ru.lanolin.compmath.l2.integrals.Integral;
import ru.lanolin.compmath.l2.methods.Method;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.MathContext;

public class Solver {

	/**
	 * Алгоритм поиска n для подготна под точность
	 * @param params Parameters
	 */
	public static void solve(Parameters params) {
		BigInteger N = params.getN();
		final BigDecimal A = new BigDecimal(params.getA());
		final BigDecimal B = new BigDecimal(params.getB());
		final BigDecimal rate = A.add(B.negate()).abs();
		final BigDecimal paramsAccuracy = params.getAccuracy();

		boolean negate = A.compareTo(B) < 0;
		if(A.compareTo(B) == 0){
			System.out.println("Ответ " + 0);
			return;
		}

		Method m = params.getMethod();
		Integral in = params.getIntegral();
		BigDecimal lastAnswer = BigDecimal.ZERO;

		long i = 0;
		System.out.print("Таблица вычислений: \n\n");
		System.out.printf("| %4s | %10s | %22s | %23s | %23s | %23s \n",
				"i", "N", "h", "New Answer", "Last Answer", "Different");
		System.out.printf("|%s|%s|%s|%s|%s|%s\n",
				"-".repeat(6), "-".repeat(12), "-".repeat(24), "-".repeat(25), "-".repeat(25), "-".repeat(25));

		while(true) {
			BigDecimal h = rate.divide(new BigDecimal(N), MathContext.DECIMAL128);

			BigDecimal newAnswer = m.area(in, B, A, h);
			if(negate) newAnswer = newAnswer.negate();

			BigDecimal diff = lastAnswer.add(newAnswer.negate()).abs();

			System.out.printf("| %4d | %10d | %3.20f | % 3.20f | % 3.20f | % 3.20f \n",
					++i, N, h, newAnswer, lastAnswer, diff);

			if(diff.compareTo(paramsAccuracy) < 1){
				lastAnswer = newAnswer;
				break;
			}else {
				N = N.shiftLeft(1);
				lastAnswer = newAnswer;
			}
		}

		System.out.printf(
				"\n\n%s на промежутке [%.5f; %.5f] достигает результата %s при точности %f на %d разбиениях",
				in.toString(), B, A, lastAnswer, paramsAccuracy, N
		);
	}
}
