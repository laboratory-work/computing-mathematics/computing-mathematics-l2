package ru.lanolin.compmath.l2.integrals;

import java.math.BigDecimal;

public interface Integral {
	BigDecimal ONE = BigDecimal.ONE;
	BigDecimal TWO = BigDecimal.valueOf(2);
	BigDecimal THREE = BigDecimal.valueOf(3);
	BigDecimal FOUR = BigDecimal.valueOf(4);
	BigDecimal FIVE = BigDecimal.valueOf(5);

	BigDecimal solve(BigDecimal x);
}
