package ru.lanolin.compmath.l2.integrals;

import java.math.BigDecimal;

public class IntegralV3 implements Integral {

	@Override
	public BigDecimal solve(BigDecimal x) {
		return x.pow(2).add(ONE.negate()).pow(2);
	}

	@Override
	public String toString() {
		return "Integral (x^2 - 1)^2 dx";
	}
}
