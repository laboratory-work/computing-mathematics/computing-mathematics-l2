package ru.lanolin.compmath.l2.integrals;

public class Integrals {

	public static final int MAX_Integral = 5;

	public static String getIntegralString(int number){
		return getIntegral(number).toString();
	}

	public static Integral getIntegral(int number){
		switch (number){
			default:
			case 1:
				return new IntegralV1();
			case 2:
				return new IntegralV2();
			case 3:
				return new IntegralV3();
			case 4:
				return new IntegralV4();
			case 5:
				return new IntegralV5();
		}
	}
}
