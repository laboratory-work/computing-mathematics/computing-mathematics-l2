package ru.lanolin.compmath.l2.integrals;

import java.math.BigDecimal;

public class IntegralV2 implements Integral {

	@Override
	public BigDecimal solve(BigDecimal x) {
		return x.multiply(BigDecimal.ONE.add(x.multiply(TWO).negate()).pow(2));
	}

	@Override
	public String toString() {
		return "Integral x * (1 - 2*x)^2 dx";
	}
}
