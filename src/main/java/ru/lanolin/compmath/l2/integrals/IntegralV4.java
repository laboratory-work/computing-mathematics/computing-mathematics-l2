package ru.lanolin.compmath.l2.integrals;

import java.math.BigDecimal;
import java.math.MathContext;

public class IntegralV4 implements Integral {

	@Override
	public BigDecimal solve(BigDecimal x) {
		return ONE.divide(x.pow(2), MathContext.DECIMAL128);
	}

	@Override
	public String toString() {
		return "Integral 1 / x^2 dx";
	}
}
