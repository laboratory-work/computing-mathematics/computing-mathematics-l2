package ru.lanolin.compmath.l2.integrals;

import java.math.BigDecimal;

public class IntegralV1 implements Integral {

	@Override
	public BigDecimal solve(BigDecimal x) {
		return x.pow(2);
	}

	@Override
	public String toString() {
		return "Integral x^2 dx";
	}
}
