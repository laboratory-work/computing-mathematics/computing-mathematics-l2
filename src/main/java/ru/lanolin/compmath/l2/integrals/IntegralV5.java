package ru.lanolin.compmath.l2.integrals;

import java.math.BigDecimal;

import static ru.lanolin.compmath.l2.utils.BigDecimalUtils.*;

public class IntegralV5 implements Integral {

	@Override
	public BigDecimal solve(BigDecimal x) {
		return sine(x).pow(3).multiply(cosine(x));
	}

	@Override
	public String toString() {
		return "Integral  sin[x]^3 * cos[x] dx";
	}
}
