package ru.lanolin.compmath.l2.implement;

import ru.lanolin.compmath.l2.utils.Parameters;

public interface InputConsole {
	 Parameters inputConsole();
}
