package ru.lanolin.compmath.l2.implement;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import ru.lanolin.compmath.l2.utils.Parameters;
import ru.lanolin.compmath.l2.integrals.Integrals;
import ru.lanolin.compmath.l2.methods.Methods;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Objects;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class InputConsoleImpl implements InputConsole {

	private static InputConsole inputConsole = null;

	public static InputConsole  getInputConsole(){
		return Objects.isNull(inputConsole) ? (inputConsole = new InputConsoleImpl()) : inputConsole;
	}

	@Override
	public Parameters inputConsole(){
		try(BufferedReader br = new BufferedReader(new InputStreamReader(System.in))){
			Parameters param = new Parameters();

			enterIntegral(br, param);
			enterMethod(br, param);
			enterUpperBorder(br, param);
			enterBottomsBorder(br, param);
			enterAccuracy(br, param);

			return param;
		} catch (IOException e) {
			System.err.println("Ошибка при вводе данных. " + e.getLocalizedMessage());
			System.err.flush();
			return null;
		}
	}

	private void enterIntegral(BufferedReader br, Parameters param) throws IOException {
		System.out.print(getFirstMenu());
		while(true) {
			try {
				System.out.print("Введите номер интеграла [1]: ");
				String str = br.readLine();
				if(str.isBlank() || str.isEmpty()){
					param.setIntegral(Integrals.getIntegral(1));
					break;
				}
				int number = Integer.parseInt(str);
				if(number < 1 || number > Integrals.MAX_Integral){
					throw new ArrayIndexOutOfBoundsException("Неверно введен номер интеграла");
				}
				param.setIntegral(Integrals.getIntegral(number));
				break;
			}catch (NumberFormatException | ArrayIndexOutOfBoundsException ex){
				System.err.println("Ошибка в воде номера. Повторите!");
				System.err.flush();
			}
		}
	}

	private void enterMethod(BufferedReader br, Parameters param) throws IOException{
		System.out.print(getSecondMenu());
		while(true){
			try {
				System.out.print("Введите номер метода [1]: ");
				String str = br.readLine();
				if(str.isBlank() || str.isEmpty()){
					param.setMethod(Methods.getMethod(1));
					break;
				}
				int number = Integer.parseInt(str);
				if(number < 1 || number > Methods.MAX_Methods){
					throw new ArrayIndexOutOfBoundsException("Неверно введен номер метода");
				}
				param.setMethod(Methods.getMethod(number));
				break;
			}catch (NumberFormatException | ArrayIndexOutOfBoundsException ex){
				System.err.println("Ошибка в вводе номера. Повторите!");
				System.err.flush();
			}
		}
	}

	private void enterUpperBorder(BufferedReader br, Parameters param) throws IOException{
		while(true){
			try {
				System.out.print("Введите верхнюю границу интеграла [1]: ");
				String str = br.readLine();
				if(str.isBlank() || str.isEmpty()){
					param.setA(BigInteger.ONE);
					break;
				}
				param.setA(new BigInteger(str));
				break;
			}catch (NumberFormatException | ArrayIndexOutOfBoundsException ex){
				System.err.println("Ошибка в вводе номера. Повторите!");
				System.err.flush();
			}
		}
	}

	private void enterBottomsBorder(BufferedReader br, Parameters param) throws IOException{
		while(true){
			try {
				System.out.print("Введите нижнюю границу интеграла [0]: ");
				String str = br.readLine();
				if(str.isBlank() || str.isEmpty()){
					param.setB(BigInteger.ZERO);
					break;
				}
				param.setB(new BigInteger(str));
				break;
			}catch (NumberFormatException | ArrayIndexOutOfBoundsException ex){
				System.err.println("Ошибка в вводе номера. Повторите!");
				System.err.flush();
			}
		}
	}

	private void enterAccuracy(BufferedReader br, Parameters param) throws IOException{
		while(true){
			try {
				System.out.print("Введите точность вычисления [0.01]: ");
				String str = br.readLine();
				if(str.isBlank() || str.isEmpty()){
					param.setAccuracy(new BigDecimal("0.01"));
					break;
				}
				param.setAccuracy(new BigDecimal(str));
				break;
			}catch (NumberFormatException | ArrayIndexOutOfBoundsException ex){
				System.err.println("Ошибка в вводе номера. Повторите!");
				System.err.flush();
			}
		}
	}

	private String getFirstMenu(){
		StringBuilder str = new StringBuilder("Доступные интегралы для вычисления:\n");
		for(int i = 0; i < Integrals.MAX_Integral; i++){
			str.append(String.format("\t[%d]. %s\n", i + 1, Integrals.getIntegralString(i+1)));
		}
		return str.toString();
	}

	private String getSecondMenu(){
		StringBuilder str = new StringBuilder("Доступные методы для вычисления: \n");
		for(int i = 0; i < Methods.MAX_Methods; i++){
			str.append(String.format("\t[%d]. %s\n", i + 1, Methods.getMethodString(i+1)));
		}
		return str.toString();
	}
}
